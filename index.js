const str = `
digraph G {
  node[shape="rectangle"]
  example [label="No graph provided"]
}
`

const inputWidth = 200;

const margin = {
    top: 20,
    bottom: 20,
    left: 20,
    right: 20,
};
const graphWidth = window.innerWidth - inputWidth - margin.left - margin.right;
const graphHeight = window.innerHeight - margin.top - margin.bottom;

const Interval = 750;

var t = d3.transition()
    .duration(Interval)
    .ease(d3.easeLinear);

const graph = d3.select("#graph")
    .graphviz({ width: graphWidth, height: graphHeight, fit: true })
    .transition(t)
    .renderDot(str);

const area = document.querySelector('#json');

area.oninput = async (e) => {
    console.log("on input!");
    const text = e.target.value;
    const vertices = text.split('$');
    for(const v of vertices) {
        console.log(vertices.length);
        setInterval(() => {
            graph.renderDot(v);
        }, 2*Interval);
    }
}