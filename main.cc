#include <iostream>
#include <boost/graph/graphviz.hpp>

enum class LastState { Unknown, Omitted, Activated, Success, Failure };
enum class Shape { Circle, Ellipse, Rectangle };

template <class G>
class NodeLabelWriter {
public:
    explicit NodeLabelWriter (G g) : g_(g) {}

    [[nodiscard]] std::string getShape(Shape s) const {
        switch (s) {
            case Shape::Circle:
                return "circle";
            case Shape::Rectangle:
                return "rectangle";
            default:
                return "ellipse";
        }
    }

    [[nodiscard]] std::string getFillColor(LastState ls) const {
        switch (ls) {
            case LastState::Unknown:
                return "yellow";
            case LastState::Omitted:
                return "white";
            case LastState::Activated:
                return "gray";
            case LastState::Success:
                return "green";
            case LastState::Failure:
                return "red";
            default:
                return "purple";
        }
    }

    template <class Vertex>
    void operator()(std::ostream &out, const Vertex &v) const {
        out << "[label=\"" << g_[v].name << "\"";
        auto shape = getShape(g_[v].shape);
        out << "shape=" << std::quoted(shape) << " ";
        auto fill_color = getFillColor(g_[v].last_state);
        out << "fillcolor=" << std::quoted(fill_color);
        out << "]";
    }

private:
    G g_;
};

template <class G>
class LinkLabelWriter {
public:
    explicit LinkLabelWriter(G _name) : g_(_name) {}
    template <class Edge>
    void operator()(std::ostream &out, const Edge &e) const {
        out << "[penwidth=\"" << g_[e].weight << "\"]";
    }

private:
    G g_;
};

template <class G>
class GraphLabelWriter {
public:
    GraphLabelWriter (G g)
            : g_(g) {}
    void operator()(std::ostream &out) const {
        out << "labelloc=\"t\"\n";
        out << "label=" << std::quoted(boost::get_property(g_).graph_name) << "\n";
        out << R"(node[style="filled" fillcolor="white"])"
            << "\n";
    }

private:
    G g_;
};

template <typename G>
void print(G const& g) {
    boost::write_graphviz(std::cout, g,
                          NodeLabelWriter<G>(g),
                          LinkLabelWriter<G>(g),
                          GraphLabelWriter<G>(g));
}

struct VertexProperties {
    std::string name;
    LastState last_state;
    Shape shape;
};

struct EdgeProperties {
    double weight;
};

struct GraphProperties {
    std::string graph_name;
};

using Graph = boost::adjacency_list<boost::vecS, boost::vecS, boost::bidirectionalS, VertexProperties, EdgeProperties, GraphProperties>;
using Vertex = boost::graph_traits<Graph>::vertex_descriptor;

int main() {
    std::vector<VertexProperties> vertices{
            {"A", LastState::Activated, Shape::Ellipse},
            {"B", LastState::Activated, Shape::Circle},
            {"C", LastState::Omitted, Shape::Ellipse},
            {"D", LastState::Omitted, Shape::Ellipse},
            {"E", LastState::Success, Shape::Rectangle}
    };
    std::vector<std::pair<std::pair<int, int>, double>> edges {
            {{0, 1}, 3.0},
            {{1, 2}, 2.0},
            {{2, 3}, 1.5},
            {{2, 4}, 3.0},
    };
    Graph graph;

    boost::get_property(graph) = GraphProperties{"FIRST GRAPH"};
    for(auto const& p : vertices) {
        auto v = boost::add_vertex(graph);
        graph[v] = p;
    }
    for(auto const& [edge, weight] : edges) {
        auto e = boost::add_edge(edge.first, edge.second, graph);
        graph[e.first] = {weight};
    }
    print(graph);
    std::cout << "\n$\n";

    boost::get_property(graph) = GraphProperties{"SECOND GRAPH"};
    auto v = boost::add_vertex(graph);
    graph[v] = {"F", LastState::Failure, Shape::Rectangle};
    auto e = boost::add_edge(0, 5, graph);
    graph[e.first] = {2.3};

    print(graph);
    return 0;
}
