### Purpose
C++ app presents how to use boost::graph to generate grapviz dot code. 
This output code can be then rendered in the page. The example `index.html` and `index.js` are present in the repository.
### Usage

To compile and run C++ executable type:
```
cmake --preset=default
cmake --build --preset=default
./build/graphviz
```

To open browser page type:
```
http-server 
```
and then open browser at page `http://127.0.0.1:8080`.

Paste output of `graphviz` to text area to see visualisation.
